// MIT License
// Author: Nick Van Osta

import color from '@mx/color';

import { exec } from '#u';
import { expect } from 'chai';

describe('Mixin - Color', () => {
  it('should correctly identify a light color', async () => {
    const col = '#DEA402';
    const isDark = exec(color.methods.isDark, {}, col);
    expect(isDark).to.be.false;
  });

  it('should correctly identify a dark color', async () => {
    const col = '#8F0000';
    const isDark = exec(color.methods.isDark, {}, col);
    expect(isDark).to.be.true;
  });
});
