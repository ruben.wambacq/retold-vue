// MIT License
// Author: Nick Van Osta

import theme from '@mx/theme';

import { exec } from '#u';
import { expect } from 'chai';
import defaultTheme from '@/plugins/theme';

const newTheme = {
  light: {
    primary: 1,
    secondary: 2,
    accent: 3,
    background: 8,
    error: 4,
    info: 5,
    success: 6,
    warning: 7
  },
  dark: {
    primary: 1,
    secondary: 2,
    accent: 3,
    background: 8,
    error: 4,
    info: 5,
    success: 6,
    warning: 7
  }
};

const createLocalThis = () => ({
  $vuetify: {
    theme: {
      themes: defaultTheme,
      defaults: defaultTheme
    }
  }
});

describe('Mixin - Theme', () => {
  it('should correctly set the theme', async () => {
    const localThis = createLocalThis();
    exec(theme.methods.setTheme, localThis, newTheme);
    expect(localThis.$vuetify.theme.themes).to.deep.equal(newTheme);
  });

  it('should correctly set only the light theme', async () => {
    const localThis = createLocalThis();
    const newThemeLight = { light: newTheme.light };
    exec(theme.methods.setTheme, localThis, newThemeLight);
    expect(localThis.$vuetify.theme.themes.light).to.deep.equal(newTheme.light);
    expect(localThis.$vuetify.theme.themes.dark).to.deep.equal(
      defaultTheme.dark
    );
  });

  it('should correctly set only the dark theme', async () => {
    const localThis = createLocalThis();
    const newThemeDark = { dark: newTheme.dark };
    exec(theme.methods.setTheme, localThis, newThemeDark);
    expect(localThis.$vuetify.theme.themes.light).to.deep.equal(
      defaultTheme.light
    );
    expect(localThis.$vuetify.theme.themes.dark).to.deep.equal(newTheme.dark);
  });

  it('should keep the original theme if the given theme is falsey', async () => {
    const localThis = createLocalThis();
    const newThemeDark = null;
    exec(theme.methods.setTheme, localThis, newThemeDark);
    expect(localThis.$vuetify.theme.themes).to.deep.equal(defaultTheme);
  });

  it('should correctly reset the theme', async () => {
    const localThis = createLocalThis();
    exec(theme.methods.resetTheme, localThis);
    expect(localThis.$vuetify.theme.themes).to.deep.equal(defaultTheme);
  });
});
