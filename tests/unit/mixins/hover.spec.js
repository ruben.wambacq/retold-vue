// MIT License
// Author: Nick Van Osta

import hover from '@mx/hover';

import { exec } from '#u';
import { expect } from 'chai';

describe('Mixin - Hover', () => {
  it('It should correctly track the hover status', async () => {
    const localThis = { hover: null };

    exec(hover.methods.mouseEnter, localThis);
    expect(localThis.hover).to.be.true;

    exec(hover.methods.mouseLeave, localThis);
    expect(localThis.hover).to.be.false;
  });
});
