module.exports = {
  root: true,

  env: {
    node: true
  },

  extends: [
    'plugin:vue/recommended',
    'eslint:recommended',
    '@vue/prettier',
    'plugin:eslint-plugin-vue-i18n/recommended'
  ],

  parserOptions: {
    parser: 'babel-eslint'
  },

  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',

    'vue-i18n/no-dynamic-keys': 'error',
    'vue-i18n/no-unused-keys': [
      'error',
      {
        extensions: ['.js', '.vue']
      }
    ],
    'vue-i18n/no-raw-text': [
      'error',
      {
        ignoreNodes: ['v-icon'],
        ignorePattern: '^[-—#:()&*\\s©]+$',
        ignoreText: ['Nick Van Osta']
      }
    ]
  },

  settings: {
    'vue-i18n': {
      localeDir: './locales/*.{json,json5,yaml,yml}'
    }
  },

  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        mocha: true
      }
    }
  ]
};
