// MIT License
// Author: Nick Van Osta

import { deepMerge, deepClone } from '@u/deep-object';
import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import sinon from 'sinon';

const theme = {
  primary: 'primary',
  secondary: 'secondary',
  accent: 'accent',
  info: 'info'
};

export const $vuetify = {
  theme: {
    currentTheme: theme,

    themes: {
      light: theme,
      dark: theme
    },

    defaults: {
      light: theme,
      dark: theme
    }
  },

  breakpoint: {
    mobile: false
  }
};

const DEFAULT_OPTIONS = {
  mocks: {
    $vuetify,
    $t: sinon.fake()
  },

  stubs: {
    RouterView: true,
    VApp: true,
    VMain: true
  }
};

/**
 * Wrapper function for component mounting used for unit testing components.
 *
 * @param {Component} component The Vue component to be mounted
 * @param {object} options Extra options used for component mounting
 * @returns {Wrapper} The Wrapper for the mounted component
 * @author Nick Van Osta
 */
export function mount(component, options = {}) {
  return shallowMount(
    component,
    deepMerge(deepClone(DEFAULT_OPTIONS), options)
  );
}

/**
 * Wrapper function for component mounting used for unit testing components.
 * Mounts a provider component with a default slot.
 *
 * @param {Component} component The Vue component to be mounted
 * @param {object} options Extra options used for component mounting
 * @returns {Wrapper} The Wrapper for the mounted component
 * @author Nick Van Osta
 */
export function mountProvider(component, options) {
  return mount(component, {
    ...options,
    slots: {
      default: '<p>DEFAULT SLOT</p>'
    }
  });
}

/**
 * Creates a fake Vuex store.
 *
 * @param {Vue} vue The Vue instance to be used.
 * @param {object} store The Vuex store definition.
 * @returns {Vuex.Store} The generated Vuex store.
 * @author Nick Van Osta
 */
export function fakeStore(vue, store) {
  vue.use(Vuex);
  return new Vuex.Store(store);
}

/**
 * Executes a component function without mounting it.
 * This can be used for methods, computed props, validators, ...
 *
 * @param {(...args) => *} func The component function to execute
 * @param {object} localThis A mocked `this` object to represent the component
 * @returns {*} The return value of the called function
 * @author Nick Van Osta
 */
export function exec(func, localThis = {}, ...args) {
  return func.call(localThis, ...args);
}

/**
 * Executes a component function without mounting it.
 *
 * @param {(...args) => *} func The component function to execute
 * @param {object} localThis A mocked `this` object to represent the component
 * @returns {object} Returns an dictionary of emitted events.
 * Each dictionary value holds a list of event emissions.
 * Each event emission is a list of arguments given to the event emit.
 *
 * For example:
 * {
 *  'click': [
 *    // 1st emit
 *    [ClickEvent],
 *    // 2nd emit
 *    [ClickEvent]
 *  ],
 *  'input': [
 *    [true, 1.0, 'string', [...], {...}]
 *  ]
 * }
 * @author Nick Van Osta
 */
export function checkEvents(func, localThis = {}, ...args) {
  const events = {};
  localThis.$emit = (event, ...args) => {
    events[event] = events[event] ? [...events[event], [...args]] : [[...args]];
  };
  exec(func, localThis, ...args);
  return events;
}

/**
 * Checks whether a given value is valid for the given component prop.
 *
 * @param {object} prop The component prop to validate
 * @param {*} value The value to validate
 * @returns {boolean} true when the value is valid for the prop, otherwise false
 * @author Nick Van Osta
 */
export function validate(prop, value) {
  if (!prop.validator) throw new Error(`Property does not have a validator`);
  return prop.validator(value);
}

/**
 * Creates a mock commit function for the given module and state.
 *
 * @param {object} module The module to create the commit for.
 * @param {object} state The state used in the commit function.
 * @returns {(mut: string, ...args) => *} The generated mock commit function.
 */
export function createCommit(module, state) {
  return (mut, ...args) => module.mutations[mut](state, ...args);
}
