// MIT License
// Author: Nick Van Osta

import CardItem from '@u/card-item';
import World from '@m/world';

import { expect } from 'chai';

describe('Utility - CardItem', () => {
  describe('constructor', () => {
    it('should create a new CardItem correctly', async () => {
      const id = 1;
      const title = 'title';
      const img = 'img';

      const card = new CardItem(id, title, img);
      expect(card.id).to.equal(id);
      expect(card.title).to.equal(title);
      expect(card.img).to.equal(img);
    });

    it('should create a new CardItem correctly without optional params', async () => {
      const id = 1;
      const title = 'title';

      const card = new CardItem(id, title);
      expect(card.id).to.equal(id);
      expect(card.title).to.equal(title);
      expect(card.img).to.be.a('string');
    });
  });

  describe('toObject', () => {
    it('should do nothing when the argument is not supported', async () => {
      const obj = { id: 0, title: 'title', img: 'img' };
      const card = CardItem.toCardItem(obj);
      expect(card).to.not.exist;
    });

    it('should generate new card item from a world object', async () => {
      const world = new World(0, 'world', 'thumbnail', 'description');
      const card = CardItem.toCardItem(world);

      expect(card.id).to.equal(world.id);
      expect(card.title).to.equal(world.name);
      expect(card.img).to.equal(world.thumbnail);
    });
  });
});
