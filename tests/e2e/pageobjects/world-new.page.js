// MIT License
// Author: Nick Van Osta

const WorldDialogPage = require('./.world-dialog.page');

class WorldNew extends WorldDialogPage {
  open() {
    return super.open('/worlds/new');
  }
}

module.exports = new WorldNew();
