#!/usr/bin/env node

const shell = require('shelljs');

const MSG_FORMAT =
  /^(fix|feat|build|chore|ci|docs|style|refactor|perf|test)(\(.+\))?!?: .+$/;
const MSG = shell.head({ '-n': 1 }, process.env.GIT_PARAMS).stdout.trim();

if (!MSG.match(MSG_FORMAT)) {
  console.info(`
Commits MUST be prefixed with a valid type, followed by the OPTIONAL scope, OPTIONAL !, and REQUIRED terminal colon and space.

Valid types:
    - fix
    - feat
    - build
    - chore
    - ci
    - docs
    - style
    - refactor
    - perf
    - test

Examples:
    feat: this is my commit

    fix!: this is my breaking commit

    chore(scope): this is my scoped commit

    perf(scope)!: this is my scoped breaking commit
`);
  shell.exit(1);
}
