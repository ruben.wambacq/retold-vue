// MIT License
// Author: Nick Van Osta

import WorldDetails from '@p/WorldDetails';

import World from '@m/world';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider } from '#u';
import { expect } from 'chai';

describe('Component - WorldDetails', () => {
  const localVue = createLocalVue();

  it('should successfully mount the component', async () => {
    const world = new World(1, 'TEST', 'THUMBNAIL', 'DESCRIPTION', 'THEME');

    const wrapper = mountProvider(WorldDetails, {
      localVue,

      propsData: {
        world
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;

    expect(wrapper.vm.state.world).to.equal(world);
  });
});
