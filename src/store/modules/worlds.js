// MIT License
// Autor: Nick Van Osta

import Vue from 'vue';

import World from '@m/world';

export const WORLDS_MODULE = 'worlds';

// Getters
export const GET_WORLD = 'get-world';

// Actions
export const ADD_WORLD = 'add-world';
export const SET_WORLD = 'set-world';
export const DEL_WORLD = 'del-world';

// Mutations
const WORLD_MUT_ADD = 'add-world';
const WORLD_MUT_SET = 'set-world';
const WORLD_MUT_DEL = 'del-world';

// Defaults
const DEFAULT_WORLDS = {
  0: new World(
    0,
    'Elder Scrolls',
    'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages-wixmp-ed30a86b8c4ca887773594c2.wixmp.com%2Ff%2F019c0b9f-a8fe-4b05-848c-562b12140d28%2Fdbga9b7-6cfa1f9e-fc33-4400-8794-28653624fea0.png%2Fv1%2Ffill%2Fw_1600%2Ch_900%2Cq_80%2Cstrp%2Felder_scrolls__flag_of_the_mede_empire_by_okiir_dbga9b7-fullview.jpg%3Ftoken%3DeyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9OTAwIiwicGF0aCI6IlwvZlwvMDE5YzBiOWYtYThmZS00YjA1LTg0OGMtNTYyYjEyMTQwZDI4XC9kYmdhOWI3LTZjZmExZjllLWZjMzMtNDQwMC04Nzk0LTI4NjUzNjI0ZmVhMC5wbmciLCJ3aWR0aCI6Ijw9MTYwMCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.736y1A-Y2nUhzPlX7MfPuxV4aUlMKtNVDqvSmQjFslU&f=1&nofb=1',
    'The world portraied in the popular Elder Scrolls franchise from Bethesda Softworks.',
    {
      light: {
        primary: '#052F5F',
        secondary: '#005377',
        accent: '#06A77D',
        background: '#FEFAE0'
      },
      dark: {
        primary: '#052F5F',
        secondary: '#005377',
        accent: '#06A77D',
        background: '#5E6572'
      }
    }
  ),
  1: new World(
    1,
    'Deus Ex Machina',
    'https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fth02.deviantart.net%2Ffs71%2FPRE%2Fi%2F2013%2F283%2F9%2Fa%2Fmedieval_town_by_shutupandwhisper-d6q07yv.jpg&f=1&nofb=1',
    'Nathan made this.',
    {
      light: {
        primary: '#8A716A',
        secondary: '#C2B8B2',
        accent: '#197BBD',
        background: '#FEFAE0'
      },
      dark: {
        primary: '#8A716A',
        secondary: '#C2B8B2',
        accent: '#197BBD',
        background: '#5E6572'
      }
    }
  ),
  2: new World(
    2,
    'The Casimir Effect',
    'https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwallpapercave.com%2Fwp%2FuOHj8nI.jpg&f=1&nofb=1',
    `The year is 2580 CE. Humanity has colonised 37 star systems with the help of wormhole technology, and after an era of turmoil the future of humanity looks bright. Still, the nations from Earth and Sol are losing their formerly uncontested dominance over interstellar power, and are unlikely to take this well. At the same time, many are still swayed by the disturbing beliefs of the previous period, and capitalism runs again rampant as corporations make their own "law". 
    ---
    In this setting, a handful of people have been deprived of everything. With no memories of yesterday and everything that came before, they must rely upon each other in this unfortunate situation. Will they find a new life, or will they attempt to uncover their past? And will the world help or hamper them on their path?`,
    {
      light: {
        primary: '#272932',
        secondary: '#4D7EA8',
        accent: '#828489',
        background: '#FEFAE0'
      },
      dark: {
        primary: '#272932',
        secondary: '#4D7EA8',
        accent: '#828489',
        background: '#5E6572'
      }
    }
  ),
  3: new World(
    3,
    'Age of Scourges',
    'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallup.net%2Fwp-content%2Fuploads%2F2017%2F03%2F29%2F489317-creepy-horror-dead_trees.jpg&f=1&nofb=1',
    `The Age of Scourges is a D&D campaign set in the world of Úmertia, a beautiful world filled with the wonders of magic, but also a great many darknesses.

    For over a hundred years, the kingdom of Oberon has been threatened by an apocalyptic terrorist group calling themselves "The Scourgers". Portals have been opening to other planes of existence, unleashing a scourge of deadly monstrous creatures into the cold and harsh lands. And a mythical group of skilled warriors, known as "The Hexxar" who are trained to find and kill The Scourgers are roaming the lands of story and reality alike.

    Now, in the year 1366 Post Second Calamity, The Scourgers and the ones that hunt them are nothing more but a myth. The world is on the brink of war and darkness is once again creeping in to blot out the sun...`,
    {
      light: {
        primary: '#ED6A5A',
        secondary: '#F4F1BB',
        accent: '#9BC1BC',
        background: '#FEFAE0'
      },
      dark: {
        primary: '#ED6A5A',
        secondary: '#F4F1BB',
        accent: '#9BC1BC',
        background: '#5E6572'
      }
    }
  )
};

export default {
  namespaced: true,

  state: {
    list: DEFAULT_WORLDS
  },

  getters: {
    [GET_WORLD]: (state) => (id) => {
      return state.list[id];
    }
  },

  mutations: {
    [WORLD_MUT_ADD]: (state, world) => {
      Vue.set(state.list, world.id, world);
    },

    [WORLD_MUT_SET]: (state, world) => {
      Vue.set(state.list, world.id, world);
    },

    [WORLD_MUT_DEL]: (state, id) => {
      Vue.delete(state.list, id);
    }
  },

  actions: {
    [ADD_WORLD]: ({ commit, state }, world) => {
      // set id
      const vals = Object.values(state.list);
      world.id = (vals.length ? vals[vals.length - 1].id : 0) + 1;
      // add to list
      commit(WORLD_MUT_ADD, world);
    },

    [SET_WORLD]: ({ commit }, world) => {
      commit(WORLD_MUT_SET, world);
    },

    [DEL_WORLD]: ({ commit }, id) => {
      commit(WORLD_MUT_DEL, id);
    }
  }
};
