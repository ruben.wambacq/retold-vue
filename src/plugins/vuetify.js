// MIT License
// Author: Nick Van Osta

import Vue from 'vue';
import Vuetify from 'vuetify';
import {
  mdiBrightness6,
  mdiPlus,
  mdiClose,
  mdiDelete,
  mdiFeather,
  mdiGitlab,
  mdiTwitter,
  mdiFacebook,
  mdiLinkedin,
  mdiEarth,
  mdiScript,
  mdiMap,
  mdiCalendar,
  mdiChartSankeyVariant,
  mdiAccount
} from '@mdi/js';

import Logo from '@/assets/logo.svg';

import theme from '@/plugins/theme';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: theme
  },

  icons: {
    iconfont: 'mdiSvg',
    values: {
      logo: {
        component: Logo
      },

      // routes
      worlds: mdiEarth,
      characters: mdiAccount,

      // footer
      gitlab: mdiGitlab,
      twitter: mdiTwitter,
      facebook: mdiFacebook,
      linkedin: mdiLinkedin,

      // article types
      article: mdiScript,
      map: mdiMap,
      calendar: mdiCalendar,
      timeline: mdiChartSankeyVariant,

      // general
      theme: mdiBrightness6,
      add: mdiPlus,
      edit: mdiFeather,
      delete: mdiDelete,
      close: mdiClose
    }
  }
});
