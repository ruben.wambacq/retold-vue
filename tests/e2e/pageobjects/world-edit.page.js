// MIT License
// Author: Nick Van Osta

const WorldDialogPage = require('./.world-dialog.page');

class WorldEdit extends WorldDialogPage {
  open() {
    return super.open('/worlds/0/edit');
  }
}

module.exports = new WorldEdit();
