// MIT License
// Author: Nick Van Osta

import CardListItem from '@p/CardListItem';

import Gradient from '@u/gradient';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider, exec, checkEvents } from '#u';
import { expect } from 'chai';
import { createSandbox } from 'sinon';

const sandbox = createSandbox();

describe('Component - CardListItem', () => {
  const localVue = createLocalVue();

  afterEach(async () => {
    sandbox.restore();
  });

  it('should successfully mount the component', async () => {
    const wrapper = mountProvider(CardListItem, {
      localVue
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should only create a gradient string when a gradient is given', async () => {
    const localThis = {
      createGradient: sandbox.fake.returns('GRADIENT'),
      $vuetify: { breakpoint: { mobile: false } },
      hover: true
    };

    // no gradient given
    let gradientString = exec(
      CardListItem.computed.state,
      localThis
    ).gradientString;
    expect(gradientString).to.be.empty;
    expect(localThis.createGradient.calledOnce).to.be.false;

    // gradient given
    localThis.createGradient = sandbox.fake.returns('GRADIENT');
    localThis.gradient = new Gradient(
      { from: '#007FFF', to: '#FF7F00' },
      { from: '0', to: '100' }
    );
    gradientString = exec(
      CardListItem.computed.state,
      localThis
    ).gradientString;
    expect(gradientString).to.not.be.empty;
    expect(localThis.createGradient.calledOnce).to.be.true;
  });

  it("should emit the 'delete' event when the item is deleted", async () => {
    const cardId = 1;
    const events = checkEvents(CardListItem.methods.delete, { cardId });

    expect(events['delete']).to.not.be.empty;
    expect(events['delete'][0]).to.not.be.empty;
    expect(events['delete'][0][0]).to.equal(cardId);
  });

  it("should emit the 'edit' event when the item is edited", async () => {
    const cardId = 1;
    const events = checkEvents(CardListItem.methods.edit, { cardId });

    expect(events['edit']).to.not.be.empty;
    expect(events['edit'][0]).to.not.be.empty;
    expect(events['edit'][0][0]).to.equal(cardId);
  });
});
