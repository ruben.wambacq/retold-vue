// MIT License
// Author: Nick Van Osta, Xander Veldeman

import CharacterDetails from '@v/CharacterDetails';

import {
  APPBAR_MODULE,
  SET_TITLE,
  SET_IMG,
  RESET_TITLE,
  RESET_IMG
} from '@xm/appbar';

import { createLocalVue } from '@vue/test-utils';
import { mount, fakeStore, exec } from '#u';
import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

describe('View - CharacterDetails', () => {
  const localVue = createLocalVue();

  beforeEach(async () => {});

  afterEach(async () => {
    sandbox.restore();
  });

  it('should successfully mount the component', async () => {
    const mockAppbarModule = {
      namespace: true,

      actions: {
        [SET_TITLE]: sandbox.fake(),
        [SET_IMG]: sandbox.fake(),
        [RESET_TITLE]: sandbox.fake(),
        [RESET_IMG]: sandbox.fake()
      }
    };

    const store = fakeStore(localVue, {
      modules: {
        [APPBAR_MODULE]: mockAppbarModule
      }
    });

    const wrapper = mount(CharacterDetails, {
      localVue,
      store,

      stubs: {
        CCharacterDetails: true
      },

      propsData: {
        id: 1
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should successfully execute the beforeCreate lifecycle hook', async () => {
    const localThis = {
      $store: {
        hasModule: () => false,
        registerModule: sandbox.fake()
      }
    };

    exec(CharacterDetails.beforeCreate[0], localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.true;

    localThis.$store.hasModule = () => true;
    localThis.$store.registerModule.resetHistory();
    exec(CharacterDetails.beforeCreate[0], localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.false;
  });

  it('should successfully execute the created lifecycle hook', async () => {
    const localThis = {
      [SET_TITLE]: sandbox.fake(),
      [SET_IMG]: sandbox.fake(),
      setTheme: sandbox.fake(),

      character: {
        name: 'test',
        race: 'test',
        gender: 'test',
        thumbnail: 'test',
        theme: {}
      }
    };
    exec(CharacterDetails.created, localThis);

    expect(localThis[SET_TITLE].calledOnce).to.be.true;
    expect(localThis[SET_IMG].calledOnce).to.be.true;
    expect(localThis.setTheme.calledOnce).to.be.true;
  });

  it('should successfully execute the mounted lifecycle hook', async () => {
    const localThis = {
      $store: {
        hasModule: () => false,
        registerModule: sandbox.fake()
      }
    };

    exec(CharacterDetails.mounted, localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.true;

    localThis.$store.hasModule = () => true;
    localThis.$store.registerModule.resetHistory();
    exec(CharacterDetails.mounted, localThis);
    expect(localThis.$store.registerModule.calledOnce).to.be.false;
  });

  it('should successfully execute the beforeDestroy lifecycle hook', async () => {
    const localThis = {
      [RESET_TITLE]: sandbox.fake(),
      [RESET_IMG]: sandbox.fake(),
      resetTheme: sandbox.fake(),

      $store: {
        unregisterModule: sandbox.fake()
      }
    };
    exec(CharacterDetails.beforeDestroy, localThis);

    expect(localThis[RESET_TITLE].calledOnce).to.be.true;
    expect(localThis[RESET_IMG].calledOnce).to.be.true;
    expect(localThis.resetTheme.calledOnce).to.be.true;
    expect(localThis.$store.unregisterModule.calledOnce).to.be.true;
  });
});
