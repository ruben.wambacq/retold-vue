// MIT License
// Author: Nick Van Osta

import Vue from 'vue';
import VueI18n from 'vue-i18n';
import locale from '@/locales/en';

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: {
    en: locale
  }
});

const loadedLanguages = ['en'];

function setI18nLanguage(lang) {
  i18n.locale = lang;
  return lang;
}

export async function loadLanguageAsync(lang) {
  if (i18n.locale == lang) return Promise.resolve(setI18nLanguage(lang));
  if (loadedLanguages.includes(lang))
    return Promise.resolve(setI18nLanguage(lang));

  const msg = await import(
    /* webpackChunkName: "lang-[request]" */ `@/locales/${lang}.json`
  );
  i18n.setLocaleMessage(lang, msg);
  loadedLanguages.push(lang);
  return setI18nLanguage(lang);
}

export default i18n;
