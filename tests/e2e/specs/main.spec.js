// MIT License
// Author: Nick Van Osta

const Main = require('../pageobjects/main.page');

const { describeE2E } = require('..');

describeE2E(
  'Main Page and Global App Components',
  () => {
    beforeEach(async () => {
      await Main.open();
    });

    it('should open and render', async () => {
      expect(Main.body).toBeDisplayed();
      expect(Main.main).toBeDisplayed();
      expect(Main.appBar).toBeDisplayed();
    });

    it('should toggle between light and dark mode when clicked on the dark/light theme button', async () => {
      const app = Main.app;
      const themeToggle = await (await Main.appBar).$('#theme-toggle');

      expect(themeToggle).toBeDisplayed();
      expect(themeToggle).toBeClickable();

      expect(app).toHaveAttribute('theme--light');
      await themeToggle.click();
      expect(app).toHaveAttribute('theme--dark');
      await themeToggle.click();
      expect(app).toHaveAttribute('theme--light');
    });
  },
  {
    mobile: () => {
      it('should render the nav-bottom, but not the nav-drawer or footer', async () => {
        expect(Main.navDrawer).not.toBeDisplayed();
        expect(Main.footer).not.toBeDisplayed();
        expect(Main.navBottom).toBeDisplayed();
      });

      it('should go to the correct page when clicking the nav buttons', async () => {
        const pageMock = browser.mock('**');
        const navLinks = await (await Main.navBottom).$$('a');

        navLinks.forEach((nl) => {
          nl.click();
          expect(browser).toHaveUrlContaining(nl.getAttribute('href'));
        });

        expect(pageMock).toBeRequested(navLinks.length);
      });
    },

    desktop: () => {
      it('should render the the nav-drawer and footer, but not the nav-bottom', async () => {
        expect(Main.navDrawer).toBeDisplayed();
        expect(Main.footer).toBeDisplayed();
        expect(Main.navBottom).not.toBeDisplayed();
      });

      it('should go to the correct page when clicking the nav buttons', async () => {
        const pageMock = browser.mock('**');
        const navLinks = await (await Main.navDrawer).$$('a');

        navLinks.forEach((nl) => {
          nl.click();
          expect(browser).toHaveUrlContaining(nl.getAttribute('href'));
        });

        expect(pageMock).toBeRequested(navLinks.length);
      });
    }
  }
);
