// MIT License
// Author: Nick Van Osta

import WorldNew from '@v/WorldNew';

import { ADD_WORLD } from '@xm/worlds';

import { createLocalVue } from '@vue/test-utils';
import { mount, exec } from '#u';
import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

describe('View - WorldNew', () => {
  const localVue = createLocalVue();

  afterEach(() => {
    sandbox.restore();
  });

  it('should successfully mount the component', async () => {
    const wrapper = mount(WorldNew, {
      localVue,

      stubs: {
        CWorldNew: true
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;
  });

  it('should correctly open/close dialog', async () => {
    const clock = sandbox.useFakeTimers();
    const localThis = { $router: { push: sinon.fake() } };

    exec(WorldNew.watch.dialog, localThis, true);
    clock.runAll();
    expect(localThis.$router.push.calledOnce).to.be.false;
    localThis.$router.push.resetHistory();

    exec(WorldNew.watch.dialog, localThis, false);
    clock.runAll();
    expect(localThis.$router.push.calledOnce).to.be.true;
  });

  it('should close the dialog when canceled', async () => {
    const localThis = { dialog: 123 };
    exec(WorldNew.methods.cancel, localThis);
    expect(localThis.dialog).to.be.false;
  });

  it('should close the dialog and save the world when saved', async () => {
    const localThis = { dialog: 123, [ADD_WORLD]: sinon.fake() };
    const world = 'TEST WORLD';
    exec(WorldNew.methods.save, localThis, world);

    expect(localThis.dialog).to.be.false;
    expect(localThis[ADD_WORLD].calledOnce).to.be.true;
  });
});
