exports.config = {
  /**
   * config for local testing with firefox
   */
  maxInstances: 1,
  services: ['geckodriver'],
  capabilities: [
    {
      browserName: 'firefox',
      acceptInsecureCerts: true,
      'moz:firefoxOptions': {
        args:
          process.argv.includes('--headless') || process.argv.includes('--ci')
            ? ['-headless']
            : []
      }
    }
  ]
};
