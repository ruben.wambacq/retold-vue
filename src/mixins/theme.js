// MIT License
// Author: Nick Van Osta

import theme from '@/plugins/theme';

export default {
  methods: {
    setTheme(newTheme) {
      if (!newTheme) return;

      // LIGHT THEME
      if (newTheme.light)
        this.$vuetify.theme.themes.light = {
          ...this.$vuetify.theme.defaults.light,
          ...theme.light,
          ...newTheme.light
        };

      // DARK THEME
      if (newTheme.dark)
        this.$vuetify.theme.themes.dark = {
          ...this.$vuetify.theme.defaults.dark,
          ...theme.dark,
          ...newTheme.dark
        };
    },

    resetTheme() {
      this.$vuetify.theme.themes = {
        light: {
          ...this.$vuetify.theme.defaults.light,
          ...theme.light
        },
        dark: {
          ...this.$vuetify.theme.defaults.dark,
          ...theme.dark
        }
      };
    }
  }
};
