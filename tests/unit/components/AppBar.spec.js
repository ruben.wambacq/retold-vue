// MIT License
// Author: Nick Van Osta

import AppBar from '@p/AppBar';

import Gradient from '@u/gradient';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider, exec, checkEvents } from '#u';
import { expect } from 'chai';
import { createSandbox } from 'sinon';

const sandbox = createSandbox();

describe('Component - AppBar', () => {
  const localVue = createLocalVue();

  afterEach(async () => {
    sandbox.restore();
  });

  it('should successfully mount the component', async () => {
    const title = 'Test Title';
    const color = 'Test color';

    const wrapper = mountProvider(AppBar, {
      localVue,

      propsData: {
        title,
        color
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;

    expect(wrapper.vm.state.title).to.equal(title);
    expect(wrapper.vm.state.color).to.equal(color);
  });

  it('should only create a gradient string when a gradient is given', async () => {
    const localThis = {
      createGradient: sandbox.fake.returns('GRADIENT')
    };

    // no gradient given
    let gradientString = exec(AppBar.computed.state, localThis).gradientString;
    expect(gradientString).to.be.empty;
    expect(localThis.createGradient.calledOnce).to.be.false;

    // gradient given
    localThis.createGradient = sandbox.fake.returns('GRADIENT');
    localThis.gradient = new Gradient(
      { from: '#007FFF', to: '#FF7F00' },
      { from: '0', to: '100' }
    );
    gradientString = exec(AppBar.computed.state, localThis).gradientString;
    expect(gradientString).to.not.be.empty;
    expect(localThis.createGradient.calledOnce).to.be.true;
  });

  it("should emit the 'theme' event when the theme is toggled", async () => {
    const events = checkEvents(AppBar.methods.toggleTheme);

    expect(events['theme']).to.not.be.empty;
    expect(events['theme'][0]).to.be.empty;
  });
});
