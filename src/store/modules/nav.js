// MIT License
// Autor: Nick Van Osta

import NavItem from '@u/nav-item';
import { WORLDS, CHARACTERS } from '@r';

// Actions
export const ADD_BOOKMARK = 'add-bookmark';
export const REMOVE_BOOKMARK = 'add-bookmark';

// Defaults
const DEFAULT_MAIN = [
  new NavItem({ name: WORLDS }, 'Worlds', '$worlds'),
  new NavItem({ name: CHARACTERS }, 'Characters', '$characters')
];
const DEFAULT_BOOKMARKS = [];

export default {
  namespaced: true,

  state: {
    main: DEFAULT_MAIN,
    bookmarks: DEFAULT_BOOKMARKS
  },

  getters: {},

  mutations: {},

  actions: {
    // TODO: bookmark stuffs
  }
};
