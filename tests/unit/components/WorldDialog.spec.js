// MIT License
// Author: Nick Van Osta

import WorldDialog from '@p/WorldDialog';

import World from '@m/world';

import { createLocalVue } from '@vue/test-utils';
import { mountProvider, exec, checkEvents } from '#u';
import { expect } from 'chai';
import { createSandbox } from 'sinon';

const sandbox = createSandbox();

describe('Component - WorldDialog', () => {
  const localVue = createLocalVue();

  afterEach(async () => {
    sandbox.restore();
  });

  it('should successfully mount the component', async () => {
    const title = 'TITLE';
    const color = 'COLOR';

    const wrapper = mountProvider(WorldDialog, {
      localVue,

      propsData: {
        title,
        color
      }
    });

    expect(wrapper.exists()).to.be.true;
    expect(wrapper.html()).to.not.be.empty;
    expect(wrapper.isVisible()).to.be.true;

    expect(wrapper.vm.state.color).to.equal(color);
    expect(wrapper.vm.state.title).to.equal(title);
  });

  it('should successfully validate the world name', async () => {
    const localThis = {
      $t: sandbox.fake(),
      world: new World()
    };
    const valid = 'World Name';
    const invalid = '';

    exec(WorldDialog.data, localThis).nameRules.forEach((rule) => {
      expect(rule(valid)).to.be.true;
      expect(rule(invalid)).to.be.a('string');
    });
  });

  it('should emit the input event correctly', async () => {
    const localThis = { value: false };
    const events = checkEvents(WorldDialog.methods.input, localThis);

    expect(events['input']).to.not.be.empty;
    expect(events['input'][0]).to.not.be.empty;
    expect(events['input'][0][0]).to.be.true;
  });

  it('should emit the cancel event correctly', async () => {
    const localThis = {
      setThumbnail: sandbox.fake(),
      newWorld: new World(0, 'WORLD', 'THUMBNAIL'),
      originalThumbnail: 'THUMBNAIL'
    };
    let events = checkEvents(WorldDialog.methods.cancel, localThis);

    expect(events['cancel']).to.not.be.empty;
    expect(events['cancel'][0]).to.be.empty;
    expect(localThis.setThumbnail.calledOnce).to.be.false;

    localThis.newWorld.thumbnail = 'IMG';
    events = checkEvents(WorldDialog.methods.cancel, localThis);

    expect(events['cancel']).to.not.be.empty;
    expect(events['cancel'][0]).to.be.empty;
    expect(localThis.setThumbnail.calledOnce).to.be.true;
  });

  it('should emit the save event correctly', async () => {
    const localThis = {
      valid: false,
      newWorld: new World(1, 'NAME', 'THUMBNAIL', 'DESCRIPTION', 'THEME')
    };
    let events = checkEvents(WorldDialog.methods.save, localThis);

    // invalid => no save
    expect(events['save']).to.not.exist;

    // valid => yes save
    localThis.valid = true;
    events = checkEvents(WorldDialog.methods.save, localThis);
    expect(events['save']).to.not.be.empty;
    expect(events['save'][0]).to.not.be.empty;
    expect(events['save'][0][0]).to.equal(localThis.newWorld);
  });

  it('should set tab correctly', async () => {
    const localThis = {
      tab: 'whatever'
    };
    const newTab = 'something else';

    exec(WorldDialog.methods.setTab, localThis, newTab);
    expect(localThis.tab).to.equal(newTab);
  });

  it('should set the world name correctly', async () => {
    const localThis = {
      newWorld: new World(1, 'NAME', 'THUMBNAIL', 'DESCRIPTION', 'THEME')
    };
    const newName = 'Snoop Dogg';

    exec(WorldDialog.methods.setName, localThis, newName);
    expect(localThis.newWorld.name).to.equal(newName);
  });

  it('should set the world thumbnail correctly', async () => {
    window.URL.createObjectURL = sandbox.stub().returns('URL');
    window.URL.revokeObjectURL = sandbox.stub();

    const localThis = {
      newWorld: new World(1, 'NAME', null, 'DESCRIPTION', 'THEME')
    };
    const newThumbnail = '._.';

    // without initial thumbnail
    exec(WorldDialog.methods.setThumbnail, localThis, newThumbnail);
    expect(localThis.newWorld.thumbnail).to.not.be.empty;
    expect(window.URL.createObjectURL.calledOnce).to.be.true;
    expect(window.URL.revokeObjectURL.calledOnce).to.be.false;

    window.URL.createObjectURL.resetHistory();
    window.URL.revokeObjectURL.resetHistory();

    // with initial thumbnail
    exec(WorldDialog.methods.setThumbnail, localThis, newThumbnail);
    expect(localThis.newWorld.thumbnail).to.not.be.empty;
    expect(window.URL.createObjectURL.calledOnce).to.be.true;
    expect(window.URL.revokeObjectURL.calledOnce).to.be.true;

    window.URL.createObjectURL.resetHistory();
    window.URL.revokeObjectURL.resetHistory();

    // set to null
    exec(WorldDialog.methods.setThumbnail, localThis);
    expect(localThis.newWorld.thumbnail).to.be.null;
  });

  it('should set the world description correctly', async () => {
    const localThis = {
      newWorld: new World(1, 'NAME', 'THUMBNAIL', 'DESCRIPTION', 'THEME')
    };
    const newDescription = 'Smoke weed every day';

    exec(WorldDialog.methods.setDescription, localThis, newDescription);
    expect(localThis.newWorld.description).to.equal(newDescription);
  });

  it('should set the selected color theme correctly', async () => {
    const localThis = {
      currentColor: 0
    };
    const newColor = 1;

    exec(WorldDialog.methods.setCurrentColor, localThis, newColor);
    expect(localThis.currentColor).to.equal(newColor);
  });

  it('should set the selected color correctly', async () => {
    const localThis = {
      currentMode: 0
    };
    const newMode = 1;

    exec(WorldDialog.methods.setThemeMode, localThis, newMode);
    expect(localThis.currentMode).to.equal(newMode);
  });

  it('should set the world theme color correctly', async () => {
    const selectedThemeMode = 'light';
    const selectedThemeColor = 'primary';
    const localThis = {
      selectedThemeMode,
      selectedThemeColor,
      newWorld: new World(1, 'NAME', 'THUMBNAIL', 'DESCRIPTION', {
        [selectedThemeMode]: { [selectedThemeColor]: 'blue' }
      })
    };
    const newColor = 'purple';

    exec(WorldDialog.methods.setColor, localThis, newColor);
    expect(
      localThis.newWorld.theme[selectedThemeMode][selectedThemeColor]
    ).to.equal(newColor);
  });

  it('should get the world theme color correctly', async () => {
    const selectedThemeMode = 'light';
    const themeColors = ['primary'];
    const color = 'blue';
    const localThis = {
      selectedThemeMode,
      themeColors,
      newWorld: new World(1, 'NAME', 'THUMBNAIL', 'DESCRIPTION', {
        [selectedThemeMode]: { [themeColors[0]]: color }
      })
    };

    let queriedColor = exec(WorldDialog.methods.getColor, localThis, 0);
    expect(queriedColor).to.equal(color);

    queriedColor = exec(
      WorldDialog.methods.getColor,
      localThis,
      themeColors[0]
    );
    expect(queriedColor).to.equal(color);

    queriedColor = exec(WorldDialog.methods.getColor, localThis, []);
    expect(queriedColor).to.be.undefined;
  });

  it('should correctly create a gradient for the preview', async () => {
    const selectedThemeMode = 'light';
    const primary = 'red';
    const accent = 'yellow';
    const localThis = {
      createGradient: sandbox.fake.returns('GRADIENT'),
      selectedThemeMode,
      newWorld: new World(1, 'NAME', null, 'DESCRIPTION', {
        [selectedThemeMode]: { primary, accent }
      })
    };

    // no world thumbnail
    let gradientString = exec(WorldDialog.computed.gradientString, localThis);
    expect(gradientString).to.be.empty;
    expect(localThis.createGradient.calledOnce).to.be.false;

    // gradient given
    localThis.createGradient = sandbox.fake.returns('GRADIENT');
    localThis.newWorld.thumbnail = 'IMG';

    gradientString = exec(WorldDialog.computed.gradientString, localThis);
    expect(gradientString).to.not.be.empty;
    expect(localThis.createGradient.calledOnce).to.be.true;
  });

  it('should set the world validity correctly', async () => {
    const localThis = {
      valid: false
    };
    const newValidity = '._.';

    exec(WorldDialog.methods.validate, localThis, newValidity);
    expect(localThis.valid).to.equal(newValidity);
  });
});
