// MIT License
// Author: Nick Van Osta

const Page = require('./.page');
class Main extends Page {
  get appBar() {
    return $('#app-bar');
  }

  get navDrawer() {
    return $('#nav-drawer');
  }

  get navBottom() {
    return $('#nav-bottom');
  }

  get footer() {
    return $('#footer');
  }

  get main() {
    return $('#main');
  }

  open() {
    return super.open('/');
  }
}

module.exports = new Main();
