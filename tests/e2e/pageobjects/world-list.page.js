// MIT License
// Author: Nick Van Osta

const Page = require('./.page');
class WorldList extends Page {
  get list() {
    return $('#world-list');
  }

  get worlds() {
    return $$('a[id^="world"]');
  }

  get addItem() {
    return $('#world-add-item');
  }

  get addFab() {
    return $('#world-add-fab');
  }

  get add() {
    return $('[id^="world-add-"]');
  }

  get delete() {
    return $('#world-delete');
  }

  get deleteInput() {
    return $('#delete-input-title');
  }

  get deleteCancel() {
    return $('#delete-cancel');
  }

  get deleteClose() {
    return $('#delete-close');
  }

  get deleteConfirm() {
    return $('#delete-confirm');
  }

  open() {
    return super.open('/worlds');
  }
}

module.exports = new WorldList();
