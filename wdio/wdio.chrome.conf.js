exports.config = {
  /**
   * config for local testing with chrome
   */
  maxInstances: 1,
  services: ['chromedriver'],
  capabilities: [
    {
      browserName: 'chrome',
      acceptInsecureCerts: true,
      'goog:chromeOptions': {
        args: ['--disable-infobars']
          .concat(
            process.argv.includes('--headless') || process.argv.includes('--ci')
              ? ['--headless', '--disable-gpu', '--disable-dev-shm-usage']
              : []
          )
          .concat(
            process.argv.includes('--ci')
              ? ['--no-sandbox', '--disable-setuid-sandbox']
              : []
          )
      }
    }
  ]
};
