// MIT License
// Author: Nick Van Osta

module.exports = class Page {
  constructor() {
    this.title = 'My Page';
  }

  get body() {
    return $('body');
  }

  get app() {
    return $('#app');
  }

  open(path) {
    return browser.url(path);
  }
};
