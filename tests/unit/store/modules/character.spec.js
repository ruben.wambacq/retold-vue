// MIT License
// Author: Xander Veldeman

import Character from '@m/character';
import characters, { GET_CHARACTER } from '@xm/characters';

import { expect } from 'chai';
import sinon from 'sinon';

const sandbox = sinon.createSandbox();

const createState = () => ({ list: {} });
const bogusCharacter = new Character(
  0,
  'Rick Astley',
  'Human',
  'Troll',
  '',
  'He will never give you up!',
  null
);

describe('Vuex - Characters', () => {
  afterEach(async () => {
    sandbox.restore();
  });

  it('GET_CHARACTER gets a character', async () => {
    const state = createState();
    state.list[0] = bogusCharacter;

    const stateCharacter = characters.getters[GET_CHARACTER](state)(0);
    expect(stateCharacter).to.equal(bogusCharacter);
  });
});
