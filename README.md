# Retold Vue.js

![retold.io logo](public/img/icons/mstile-150x150.png)

**Production:**
[![pipeline status](https://gitlab.com/retold-io/retold-vue/badges/prod/pipeline.svg)](https://gitlab.com/retold-io/retold-vue/-/commits/prod)
[![coverage report](https://gitlab.com/retold-io/retold-vue/badges/prod/coverage.svg)](https://gitlab.com/retold-io/retold-vue/-/commits/prod)

**Development:**
[![pipeline status](https://gitlab.com/retold-io/retold-vue/badges/dev/pipeline.svg)](https://gitlab.com/retold-io/retold-vue/-/commits/dev)
[![coverage report](https://gitlab.com/retold-io/retold-vue/badges/dev/coverage.svg)](https://gitlab.com/retold-io/retold-vue/-/commits/dev)

## About

Retold.io is a worldbuilding app made to manage and organize the worlds and stories floating around your imagination.

Manage your custom worlds, characters, timelines, maps, and more, and link them together to create **your** very own stories.

## Development

See [CONTRIBUTING](CONTRIBUTING.md).
