const path = require('path');

const publicPath = process.env.BASE_URL;

module.exports = {
  transpileDependencies: ['vuetify'],

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  },

  publicPath,

  configureWebpack: {
    resolve: {
      alias: {
        '@c': path.resolve(__dirname, 'src/components/consumer'),
        '@p': path.resolve(__dirname, 'src/components/provider'),
        '@v': path.resolve(__dirname, 'src/views'),
        '@mx': path.resolve(__dirname, 'src/mixins'),
        '@m': path.resolve(__dirname, 'src/model'),
        '@u': path.resolve(__dirname, 'src/util'),
        '@x': path.resolve(__dirname, 'src/store'),
        '@xm': path.resolve(__dirname, 'src/store/modules'),
        '@r': path.resolve(__dirname, 'src/router'),
        '#': path.resolve(__dirname, 'tests'),
        '#u': path.resolve(__dirname, 'tests/unit')
      }
    }
  },

  chainWebpack: (config) => {
    if (process.env.NODE_ENV !== 'production') {
      config.module
        .rule('istanbul')
        .test(/\.(js|vue)$/)
        .post()
        .include.add(path.resolve(__dirname, '/apollo-server'))
        .add(path.resolve(__dirname, '/src'))
        .end()
        .use('istanbul-instrumenter-loader')
        .loader('istanbul-instrumenter-loader')
        .options({ esModules: true })
        .after('babel-loader')
        .end();
    }

    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader');
  }
};
